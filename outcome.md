Steps
====
1. Make to not forget setting the config

    <mvc:annotation-driven />

2. Append UTF-8 to the @RequestMapping

	@RequestMapping(value = "/", method = RequestMethod.GET, produces="application/json;UTF-8")
	
3. That is it.

Final code
====
	package test;

	import org.springframework.stereotype.Controller;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.ResponseBody;

	@Controller
	public class StringRestController {
	    @RequestMapping(value = "/string", method = RequestMethod.GET, produces="application/json;charset=UTF-8")
	    @ResponseBody
	    public String testString() {
	        return "{\"tut\":\"åäö\"}";
	    }

	    @RequestMapping(value = "/pojo", method = RequestMethod.GET, produces="application/json;charset=UTF-8")
	    @ResponseBody
	    public Pojo testPojo() {
	        return new Pojo("åäö");
	    }
	}

Outcome
====
curl -i http://localhost:8080/spring-mvc-string-rest-poc/app/string | hexdump -C

	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
	                                 Dload  Upload   Total   Spent    Left  Speed
	100    16  100    16    0     0   4043      0 --:--:-- --:--:-- --:--:--  8000
	00000000  48 54 54 50 2f 31 2e 31  20 32 30 30 20 4f 4b 0d  |HTTP/1.1 200 OK.|
	00000010  0a 43 6f 6e 74 65 6e 74  2d 54 79 70 65 3a 20 61  |.Content-Type: a|
	00000020  70 70 6c 69 63 61 74 69  6f 6e 2f 6a 73 6f 6e 3b  |pplication/json;|
	00000030  63 68 61 72 73 65 74 3d  55 54 46 2d 38 0d 0a 43  |charset=UTF-8..C|
	00000040  6f 6e 74 65 6e 74 2d 4c  65 6e 67 74 68 3a 20 31  |ontent-Length: 1|
	00000050  36 0d 0a 53 65 72 76 65  72 3a 20 4a 65 74 74 79  |6..Server: Jetty|
	00000060  28 36 2e 31 2e 32 36 29  0d 0a 0d 0a 7b 22 74 75  |(6.1.26)....{"tu|
	00000070  74 22 3a 22 c3 a5 c3 a4  c3 b6 22 7d              |t":"......"}|
	0000007c

curl -i http://localhost:8080/spring-mvc-string-rest-poc/app/pojo | hexdump -C

	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
	                                 Dload  Upload   Total   Spent    Left  Speed
	100    16    0    16    0     0   3998      0 --:--:-- --:--:-- --:--:--  8000
	00000000  48 54 54 50 2f 31 2e 31  20 32 30 30 20 4f 4b 0d  |HTTP/1.1 200 OK.|
	00000010  0a 43 6f 6e 74 65 6e 74  2d 54 79 70 65 3a 20 61  |.Content-Type: a|
	00000020  70 70 6c 69 63 61 74 69  6f 6e 2f 6a 73 6f 6e 3b  |pplication/json;|
	00000030  63 68 61 72 73 65 74 3d  55 54 46 2d 38 0d 0a 54  |charset=UTF-8..T|
	00000040  72 61 6e 73 66 65 72 2d  45 6e 63 6f 64 69 6e 67  |ransfer-Encoding|
	00000050  3a 20 63 68 75 6e 6b 65  64 0d 0a 53 65 72 76 65  |: chunked..Serve|
	00000060  72 3a 20 4a 65 74 74 79  28 36 2e 31 2e 32 36 29  |r: Jetty(6.1.26)|
	00000070  0d 0a 0d 0a 7b 22 74 75  74 22 3a 22 c3 a5 c3 a4  |....{"tut":"....|
	00000080  c3 b6 22 7d                                       |.."}|
	00000084
