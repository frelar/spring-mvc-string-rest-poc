Answer to a question I never posted on [stackoverflow](http://stackoverflow.com).
====
Return a String with Content-Type: "application/json" from a Spring MVC controller in correct UTF-8 encoding?

I have a database filled with data in JSON format.
I'd like to return this data to a JavaScript, using a Spring 3 MVC controller as a 'proxy'.

My preferred solution would look something like this

    package test;

    import org.springframework.stereotype.Controller;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RequestMethod;
    import org.springframework.web.bind.annotation.ResponseBody;

    @Controller
    public class StringRestController {
        @RequestMapping(value = "/", method = RequestMethod.GET, produces="application/json")
        @ResponseBody
        public String test() {
            return "{\"tut\":\"tut\"}";
        }
    }

When I test the code above I get the following result.

    curl -i http://localhost:8080/
    
    HTTP/1.1 200 OK
    Content-Type: text/plain; charset=iso-8859-1
    Content-Length: 13
    Server: Jetty(6.1.26)

    {"tut":"tut"}%

I have Jackson on my classpath and it is working fine and sets the correct Content-Type when I return all other type of objects except Strings.

Addendum
====
The last sentence above turned out to be untrue. When I started to fix this the rest of the solution followed.
Please see this example for a working setup.

Todo
====
Returning JSON from a String does not use chunked transfer encoding. That would be nice to have.
