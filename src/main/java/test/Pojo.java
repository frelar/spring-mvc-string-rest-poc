package test;

/**
 * @author Fredrik Larsson (N342100)
 * @since 1.0
 */
public class Pojo {
    private String tut;

    public Pojo(String tut) {
        this.tut = tut;
    }

    public String getTut() {
        return tut;
    }
}
